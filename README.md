| :warning: **WARNING!** :warning: |
|---|
| This is an anonymized version of the repository for academic peer review of our associated paper, please do not use. This repository and the linked documentation is not maintained. Please instead use this [link to the non-anonymized repository](https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/covid_pneumothorax). Please note we were unable to anonymize the LICENSE FILE due to copyright.|

Probing multinational imaging datasets with AI to understand COVID-19 pneumothorax
==================================================================================

[[_TOC_]]

Pneumothorax is an important complication of COVID-19, occurring in 1% of admitted cases. Being relatively uncommon, 
studying its pathophysiology is challenging, with relevant imaging data being distributed across disparate silos. To 
gain mechanistic insight, we used artificial intelligence (AI) at scale to identify radiological cases from four large 
imaging datasets across 26 centres in seven countries.

This repository is to accompany our research letter, which has recently been submitted:

> **Probing multinational imaging datasets with AI to understand COVID-19 pneumothorax**
> 
> *Authors Anonymized*

The CXR Pneumothorax Detection Model
-----------------------------------------

The AI model used to detect pneumothorax in the CXR images is based on the 
[DenseNet-121 architecture](https://github.com/liuzhuang13/DenseNet) and model utilised in 
[CheX-Net](https://stanfordmlgroup.github.io/projects/chexnet/).  The code is not included in this repository 
given that it is [already available on GitHub](https://github.com/jrzech/reproduce-chexnet).  The only difference 
between the model used in this project and the original is that the final layer was changed to a single output to 
allow for binary classification of pneumothorax. The model weights were then fine-tuned specifically for pneumothorax 
using the [CheXpert dataset](https://stanfordmlgroup.github.io/competitions/chexpert/), which is also avaliable 
open-source for research purposes on the 
[Stanford AIMI website](https://stanfordaimi.azurewebsites.net/datasets/8cbd9ed4-2eb9-4565-affc-111cf4f7ebe2).

Confidence intervals were calculated using bootstrapping with code provided in `scripts/analyse_ptx.py`.

COVID-19 Data
---------------

The COVID-19 data utilised in the project was obtained from the following sources:
- National COVID-19 Chest Imaging Database (NCCID) - 
[Avaliable on application to the NHS AI Lab](https://nhsx.github.io/covid-chest-imaging-database/).
- CUH - Avaliable on application to the [Research and Development department at 
*Anonymized (link removed)*]().
- MIDRC RSNA International COVID-19 Open Radiology Database dataset 1c (RICORD-1c) - Use is free to all researchers. 
[Avaliable online](https://wiki.cancerimagingarchive.net/pages/viewpage.action?pageId=70230281).
- Valencian Region Medical ImageBank (BIMCV-COVID19) - Use is free to all researchers. [Access can be requested
online](https://bimcv.cipf.es/bimcv-projects/bimcv-covid19/).

> Please note that the NCCID clinical data was cleaned with the 
> [NCCIDxClean pipeline](https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/nccidxclean).

Review Criteria
---------------

The criteria used by the team of radiologists to review the CT scans is included in the repository as an Excel
spreadsheet (`Review_Criteria.xlsx`).  This was utilised to ensure that the imaging was reviewed in a systematic and
consistent manner.

Notebooks
-----------

The repository contains two notebooks used in the analysis (`analyse_ptx.ipynb` and `analyse_ct.ipynb`).  These include 
additional tables and charts not included in the letter. These have been included to aid reproducibility and 
transparency of the analysis, whilst also providing a more detailed view of the data.

Scripts
---------

This folder contains the python code used in the notebooks to perform the analysis (`analyse_ptx.py` for the model and 
cxr analysis, `analyse_ct.py` for the CT analysis).  These files include the code to read in the data, perform the 
analysis and generate the tables and charts. `extract_dicom_metadata.py` allows for extraction of metadata from the 
DICOM (*NCCID, CUH, and Brixia*) and json files (*BIMCV*).

Note that the default paths must be used or overwritten when calling the analyse script classes.  The default paths are
provided at the end of the README.

### Incidence Adjustment

The calculated incidence of pneumothorax secondary to COVID-19 was 0.76 (95% CI [0.56%, 0.97%]). To adjust for the
model's sensitivity, the incidence was divided by the maximum sensitivity of the model (0.74 for the CUH data) 
across the three test datasets. This gave an adjusted incidence of 1.02% (95% CI [0.75%, 1.31%]). This is likely a more 
accurate estimate of the true incidence of pneumothorax secondary to COVID-19, but relies on the assumption that the 
model did not outperform this sensitivity when deployed. It is for this reason that the maximum sensitivity was used, 
rather than the mean across the three datasets. The confidence interval was calculated using bootstrapping with code in 
`scripts/analyse_ptx.py`.

### Default Paths

Should you wish to run the code in this repository, the default paths are provided below.  These can be overwritten 
when calling the analyse script classes or changed at the beginning of the python scripts themselves.

| Description                                               | Default Variable          | Default Path                                                |
|-----------------------------------------------------------|---------------------------|-------------------------------------------------------------|
| CXR Model Inference Results                               | RESULTS_PATH              | "results/cxr_inference_results.csv"                         |
| CXR Ground Truth                                          | GT_PATH                   | "results/pthx_ground_truth.csv"                             |
| CXR Results Review (*columns as in Review_Criteria.xlsx*) | REVIEW_PATH               | "results/cxr_review_results.csv"                            |
| CXR Quality Control                                       | QC_DIR                    | 'metadata/qc_data'                                          |
| NCCID DICOM Metadata                                      | NCCID_METADATA_PATH       | "metadata/dicom_metadata/nccid_dicom_metadata.csv"          |
| NCCID Clinical Data (*cleaned with NCCIDxClean*)          | NCCID_CLINICAL_DATA_PATH  | "metadata/clinical_data/nccid_clinical_data.csv"            |
| CUH DICOM Metadata                                        | CUH_METADATA_PATH         | "metadata/dicom_metadata/cuh_dicom_metadata.csv"            |
| CUH Patient Demographics                                  | CUH_DEMOGS_PATH           | "metadata/clinical_data/cuh/DEMOGS_V2022-02-11.csv"         |
| CUH Test Results                                          | CUH_TESTS_PATH            | "metadata/clinical_data/cuh/TESTS_V2022-02-11_positive.csv" |
| CUH Admissions and Discharges                             | CUH_ADT_PATH              | "metadata/clinical_data/cuh/ADT_v2022-02-11_positive.csv"   |
| BIMCV DICOM Metadata                                      | BIMCV_METADATA_PATH       | "metadata/dicom_metadata/bimcv_dicom_metadata.csv"          |
| BIMCV Patient Demographics                                | BIMCV_DEMOGS_PATH         | "metadata/clinical_data/bimcv/participants.tsv"             |
| BIMCV COVID-19 Results                                    | BIMCV_TESTS_PATH          | "metadata/clinical_data/bimcv/sil_reg_covid_posi.tsv"       |
| RICORD DICOM Metadata                                     | RICORD_METADATA_PATH      | "metadata/dicom_metadata/ricord_dicom_metadata.csv"         |
| RICORD Clinical Data                                      | RICORD_CLINICAL_DATA_PATH | "metadata/clinical_data/ricord_clinical_data.xlsx"          |
