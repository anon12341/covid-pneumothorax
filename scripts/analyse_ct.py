"""
analyse_ct.py: Analysing the results of the review of COVID-pneumothorax CT scans

This script contains the CTAnalysis class used to analyse the results of the radiologist review of COVID-pneumothorax
CTs identified from CXR scans using the AI model supplemented by radiology reports, after review by a radiologist. The
script will generate figures and tables for the paper, with additional figures and tables generated for analysis.
This script is implemented in the analyse_ct.ipynb notebook.

This code was primarily published to accompany our research letter to offer transparency and the ability to reproduce
our results. Please note that should this code be used for further analysis, the paths to the data  will need to be
updated.

Author: ANON
Contact email: ANON
Copyright: ANON - SEE LICENSE
Lisence: MIT
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.ticker as mtick
import matplotlib.pylab as pylab

pd.set_option('display.max_columns', None)

# Set directory paths --------------------------------------------------------------------------------------------------

CT_RESULTS_PATH = "./results/Final_CT_Data.csv"

# NCCID
NCCID_METADATA_PATH = "metadata/dicom_metadata/nccid_dicom_metadata.csv"
NCCID_CLINICAL_DATA_PATH = "metadata/clinical_data/nccid_clinical_data.csv"

# CUH
CUH_METADATA_PATH = "metadata/dicom_metadata/cuh_dicom_metadata.csv"
CUH_DEMOGS_PATH = "metadata/clinical_data/cuh/DEMOGS_V2022-02-11.csv"
CUH_TESTS_PATH = "metadata/clinical_data/cuh/TESTS_V2022-02-11_positive.csv"
CUH_ADT_PATH = "metadata/clinical_data/cuh/ADT_v2022-02-11_positive.csv"

# BIMCV
BIMCV_METADATA_PATH = "metadata/dicom_metadata/bimcv_dicom_metadata.csv"
BIMCV_DEMOGS_PATH = "metadata/clinical_data/bimcv/participants.tsv"
BIMCV_TESTS_PATH = "metadata/clinical_data/bimcv/sil_reg_covid_posi.tsv"

# RICORD
RICORD_METADATA_PATH = "metadata/dicom_metadata/ricord_dicom_metadata.csv"
RICORD_CLINICAL_DATA_PATH = "metadata/clinical_data/ricord_clinical_data.xlsx"

# Set matplotlib / seaborn parameters ----------------------------------------------------------------------------------

params = {'legend.fontsize': 'x-large',
          'legend.title_fontsize': 'x-large',
          'figure.figsize': (6, 6),
          'axes.labelsize': 'x-large',
          'axes.titlesize': 16,
          'figure.titlesize': 16,
          'xtick.labelsize': 'large',
          'ytick.labelsize': 'large',
          'axes.labelpad': 6,
          'axes.labelweight': 'bold',
          'font.family': 'Times New Roman',
          'font.weight': 'normal',
          'savefig.transparent': True,
          'savefig.dpi': 200
          }

pylab.rcParams.update(params)

# Change seaborn palatte
set_pal = "Set2"
# sns.set_color_codes(palette='Spectral')
pal = sns.husl_palette(10, h=0.78, s=0.9, l=0.78)
sns.set_palette(pal)
# colours = pal.as_hex()
sns.palplot(pal)


# Classes --------------------------------------------------------------------------------------------------------------

class CTAnalysis:
    def __init__(self, ct_results_path=CT_RESULTS_PATH, cuh_demogs_path=CUH_DEMOGS_PATH,
                 bimcv_demogs_path=BIMCV_DEMOGS_PATH, nccid_demogs_path=NCCID_CLINICAL_DATA_PATH):
        self.df = pd.read_csv(ct_results_path)
        self.cuh_meta = pd.read_csv(cuh_demogs_path)
        self.bimcv_meta = pd.read_csv(bimcv_demogs_path, sep='\t')
        self.nccid_meta = pd.read_csv(nccid_demogs_path)
        self._merge_metadata()
        self._create_outcome_column()
        self._set_binary_intubation_status()

    def _merge_metadata(self):
        """
        Add age, gender and outcome where possible.
        """
        self._remap_columns()

        self.df = self.df.merge(
            self.cuh_meta[['Patient_ID', 'Age', 'Gender', 'Date_of_Death']], how='left', on='Patient_ID'
        )
        self.df['Age'] = self.df['Age_x'].fillna(self.df['Age_y'])
        self.df['Gender'] = self.df['Gender_x'].fillna(self.df['Gender_y'])
        self.df = self.df.drop(['Gender_x', 'Gender_y', 'Age_x', 'Age_y'], axis=1)

        self.df = self.df.merge(self.bimcv_meta[['Patient_ID', 'Age', 'Gender']], how='left', on='Patient_ID')
        self.df['Age'] = self.df['Age_x'].fillna(self.df['Age_y'])
        self.df['Gender'] = self.df['Gender_x'].fillna(self.df['Gender_y'])
        self.df = self.df.drop(['Gender_x', 'Gender_y', 'Age_x', 'Age_y'], axis=1)

        self.df = self.df.merge(
            self.nccid_meta[['Patient_ID', 'Age', 'Gender', 'Date_of_Death']], how='left', on='Patient_ID'
        )
        self.df['Age'] = self.df['Age_x'].fillna(self.df['Age_y'])
        self.df['Gender'] = self.df['Gender_x'].fillna(self.df['Gender_y'])
        self.df['Date_of_Death'] = self.df['Date_of_Death_x'].fillna(self.df['Date_of_Death_y'])
        self.df = self.df.drop(['Gender_x', 'Gender_y', 'Age_x', 'Age_y', 'Date_of_Death_x', 'Date_of_Death_y'], axis=1)

    def _create_outcome_column(self):
        """
        Create a column to indicate whether the patient died or survived.
        """
        self.df['Death'] = ""
        self.df.loc[self.df['Date_of_Death'].notnull(), 'Death'] = 'Died'
        self.df.loc[self.df['Date_of_Death'].isnull(), 'Death'] = 'Survived'
        self.df.loc[self.df['Dataset'] == 'Valencia', 'Death'] = 'Unknown'

    def _remap_columns(self):
        """
        Rename columns to match.
        """
        cuh_dict = {'STUDY_SUBJECT_DIGEST': 'Patient_ID', 'AGE_IN_YEARS_IF_ALIVE': 'Age', 'GENDER_DESC': 'Gender',
                    'DATE_OF_DEATH': 'Date_of_Death'}
        bimcv_dict = {'participant': 'Patient_ID', 'gender': 'Gender', 'age': 'Age'}
        nccid_dict = {'Pseudonym': 'Patient_ID', 'age': 'Age', 'sex': 'Gender', 'date_of_death': 'Date_of_Death'}

        self.df = self.df.rename(columns={"Sex": "Gender"})
        self.cuh_meta = self.cuh_meta.rename(columns=cuh_dict)
        self.bimcv_meta = self.bimcv_meta.rename(columns=bimcv_dict)

        nccid_cols = list(nccid_dict.keys())
        self.nccid_meta = self.nccid_meta[nccid_cols]
        self.nccid_meta = self.nccid_meta.rename(columns=nccid_dict)

    def keep_only_selected_causes(self, causes):
        """
        Keep only the rows with the specified causes.
        """
        self.df = self.df[self.df['Likely Cause of Pneumothorax'].isin(causes)]

    def remove_selected_causes(self, causes):
        """
        Remove rows with the specified causes.
        """
        self.df = self.df[~self.df['Likely Cause of Pneumothorax'].isin(causes)]

    def _set_binary_intubation_status(self):
        self.df.loc[self.df['Intubated'] != 'No', 'ppv'] = "On PPV"
        self.df.loc[self.df['Intubated'] == 'No', 'ppv'] = "Not on PPV"

    def ct_findings_by_ppv(self, ax=None, legend=True):
        finding_df = self.df[['Patient_ID', 'Radiological Findings', 'ppv']].copy()

        findings_of_interest = [
            "Ground Glass", "Consolidation", "Irregular Bronchial Dilatation", "Reticulation", "Nodules", "Cysts"
        ]

        for finding in findings_of_interest:
            finding_df[finding] = 0
            finding_df.loc[finding_df['Radiological Findings'].str.contains(finding, na=False), finding] = 1

        no_of_ppv_cts = len(finding_df[finding_df.ppv == 'On PPV'])
        no_of_nov_cts = len(finding_df[finding_df.ppv == 'Not on PPV'])
        print("Number of CTs of PPV patients included: ", no_of_ppv_cts)
        print("Number of CTs of non-PPV patients included: ", no_of_nov_cts)

        finding_percent_cts = finding_df.pivot_table(values=findings_of_interest, columns="ppv")
        finding_percent_cts = finding_percent_cts.reset_index().rename({'index': 'Radiological Finding'}, axis=1)

        temp1 = finding_percent_cts[['Radiological Finding', 'On PPV']]
        temp1['PPV status'] = 'On PPV'
        temp2 = finding_percent_cts[['Radiological Finding', 'Not on PPV']]
        temp2['PPV status'] = 'Not on PPV'
        df = pd.concat([temp1, temp2])
        df = df.reset_index(drop=True)
        df.loc[df['On PPV'].notnull(), 'Percentage of CTs'] = df['On PPV'] * 100
        df.loc[df['On PPV'].isnull(), 'Percentage of CTs'] = df['Not on PPV'] * 100
        df['Percentage of CTs'] = df['Percentage of CTs'].astype(int)
        df.loc[
            df['Radiological Finding'] == 'Irregular Bronchial Dilatation', "Radiological Finding"
        ] = 'Irreg. Bronchial\nDilatation'

        if ax is None:
            fig, ax = plt.subplots(figsize=(8, 6))

        ax = sns.barplot(x="Radiological Finding", y="Percentage of CTs", hue="PPV status", data=df, ax=ax,
                         palette="Blues")
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha='right', rotation_mode='anchor')
        handles, labels = ax.get_legend_handles_labels()
        if not legend:
            ax.get_legend().remove()

        if ax is None:
            return df, ax
        return df, ax

    def ct_pattern_by_ppv(self, ax=None, legend=True):
        pattern_df = self.df[["Predominant Pathological Pattern", "ppv"]]
        no_of_ppv_cts = len(pattern_df[pattern_df.ppv == 'On PPV'])
        no_of_nov_cts = len(pattern_df[pattern_df.ppv == 'Not on PPV'])
        print("Number of CTs of PPV patients included: ", no_of_ppv_cts)
        print("Number of CTs of non-PPV patients included: ", no_of_nov_cts)

        pattern_df = pattern_df.sort_values('Predominant Pathological Pattern', key=lambda col: col.map({
            'Non-COVID': 4,
            'Organising Pneumonia': 1,
            'Acute Lung Injury': 2,
            'Classic COVID': 3}))

        pattern_df.loc[
            pattern_df["Predominant Pathological Pattern"] == "Acute Lung Injury", "Predominant Pathological Pattern"
        ] = "Acute Lung\nInjury"
        pattern_df.loc[
            pattern_df["Predominant Pathological Pattern"] == "Classic COVID", "Predominant Pathological Pattern"
        ] = "Peripheral GGO\n+/- Consolidation"
        pattern_df.loc[
            pattern_df["Predominant Pathological Pattern"] == "Non-COVID", "Predominant Pathological Pattern"
        ] = "Atypical for\nCOVID-19"
        pattern_df.loc[
            pattern_df["Predominant Pathological Pattern"] == "Organising Pneumonia", "Predominant Pathological Pattern"
        ] = "Organising\nPneumonia"

        pattern_df2 = pd.DataFrame(pattern_df.groupby("Predominant Pathological Pattern")["ppv"].value_counts())
        pattern_df2 = pattern_df2.rename({'ppv': 'Percentage of CTs'}, axis=1)
        pattern_df2 = pattern_df2.reset_index()
        pattern_df2.loc[
            pattern_df2['ppv'] == 'On PPV', "Percentage of CTs"
        ] = (pattern_df2["Percentage of CTs"] * 100) / no_of_ppv_cts
        pattern_df2.loc[
            pattern_df2['ppv'] == 'Not on PPV', "Percentage of CTs"
        ] = (pattern_df2["Percentage of CTs"] * 100) / no_of_nov_cts
        pattern_df2['Percentage of CTs'] = pattern_df2['Percentage of CTs'].astype(int)

        if ax is None:
            fig, ax = plt.subplots(figsize=(8, 6))

        sns.barplot(
            x='Predominant Pathological Pattern', y='Percentage of CTs', hue='ppv', data=pattern_df2, ax=ax,
            order=[
                'Acute Lung\nInjury',
                'Organising\nPneumonia',
                "Peripheral GGO\n+/- Consolidation",
                'Atypical for\nCOVID-19',
            ], palette="Blues"
        )

        ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha='right', rotation_mode='anchor')
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles=handles, labels=labels)
        ax.yaxis.set_label_text("")
        if not legend:
            ax.get_legend().remove()

        # for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        #     label.set_fontsize(13)
        # for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        #     label.set_fontsize(13)

        if ax is None:
            return pattern_df2, ax
        return pattern_df2, ax
