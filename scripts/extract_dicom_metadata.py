"""
extract_dicom_metadata.py: Extract DICOM Metadata from a Directory of DICOM Files.

This script contains utility functions for extracting metadata from DICOM files located within a specified directory.
It supports DICOM files in both .dcm and .json formats. The extracted metadata is transformed into a pandas DataFrame
for convenient data manipulation and analysis. If desired, this DataFrame can be exported to a CSV file. The script
leverages multithreading to expedite the process of metadata extraction, making it more efficient when dealing with
large datasets.

Author: ANON
Contact email: ANON
Copyright: ANON - SEE LICENSE
Lisence: MIT
"""

import pandas as pd
import numpy as np
import pydicom
from tqdm.auto import tqdm
import os
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
from typing import Dict, List
import json

# CONSTANTS -----------------------------------------------------------------------------------------------------------

DATASET = "nccid"  # "nccid", "cuh", "ricord", "bimcv"
DICOM_DIRECTORY = ""  # Path to directory containing DICOM files
FILE_TYPE = "dcm"  # "dcm" or "json"


# FUNCTIONS -----------------------------------------------------------------------------------------------------------
def pydicom_to_df(datasets: Dict) -> pd.DataFrame:
    """
    Converts a dictionary of DICOM metadata into a pandas DataFrame.

    :param datasets: Dictionary where the keys are DICOM file paths and the values are pydicom datasets.
    :type datasets: Dict
    :return: A DataFrame where each row represents metadata of a DICOM file.
    :rtype: pd.DataFrame
    """
    attributes = []
    for file, ds in datasets.items():
        record = {"Path": file}

        record.update({attribute: ds.get(attribute) for attribute in ds.dir()})
        attributes.append(record)

    return pd.DataFrame(attributes)


def create_dcm_ds_from_json(json_path):
    """
    Creates a pydicom dataset from a json file.

    :param json_path: Path to the JSON file containing DICOM metadata.
    :type json_path: str
    :return: A pydicom Dataset object if the conversion is successful; np.nan otherwise.
    :rtype: pydicom.dataset.Dataset or np.nan
    """
    with open(json_path) as f:
        meta_json = json.load(f)

    try:
        ds = pydicom.dataset.Dataset.from_json(meta_json)

    except Exception as e1:
        try:
            # For some reason the data representation of some headers is
            # "DS" = decimal string, but the value is text causing the error.
            # These are changed to "LS" = long string, which is what they usually are.
            meta_json['00281054']["vr"] = "LS"
            meta_json['20500020']["vr"] = "LS"

            ds = pydicom.dataset.Dataset.from_json(meta_json)
        except Exception as e2:
            ds = np.nan
            print("Couldn't convert", json_path)
            print("1st Error:", e1)
            print("2nd Error:", e2)

    return ds


def extract_dicom_metadata(file_path: str, file_type: str = "dcm") -> Dict:
    """
    Extracts metadata from a single DICOM file.

    :param file_path: Path to the DICOM file.
    :type file_path: str
    :param file_type: Type of the DICOM file (either 'dcm' or 'json'). Default is 'dcm'.
    :type file_type: str, optional
    :return: Dictionary where the key is the DICOM SOP Instance UID and the value is the corresponding pydicom dataset.
    :rtype: Dict
    """
    dataset = {}

    if file_type == "dcm":
        ds = pydicom.dcmread(file_path, stop_before_pixels=True)
    elif file_type == "json":
        ds = create_dcm_ds_from_json(file_path)
    else:
        raise ValueError(f"File type entry ({file_type}) not supported. Use 'dcm' or 'json'.")

    dataset[ds.SOPInstanceUID] = ds
    return dataset


def create_image_list(dcm_dir: str = DICOM_DIRECTORY, file_type: str = FILE_TYPE) -> List[str]:
    """
    Creates a list of all DICOM files in a directory.

    :param dcm_dir: Directory to search for DICOM files. Default is DICOM_DIRECTORY.
    :type dcm_dir: str, optional
    :param file_type: Type of the DICOM files (either 'dcm' or 'json'). Default is FILE_TYPE.
    :type file_type: str, optional
    :return: A list of paths to all DICOM files found in the directory.
    :rtype: List[str]
    """
    img_list = []
    for directory, subdirectories, files in tqdm(os.walk(dcm_dir), desc=f"Finding DICOMs in {dcm_dir}"):
        img_list.extend([os.path.join(directory, file) for file in files if file.endswith(f".{file_type}")])
    return img_list


def extract_metadata_from_dicoms(
        directory: str = DICOM_DIRECTORY, file_type: str = FILE_TYPE, num_threads: int = 20
) -> pd.DataFrame:
    """
        Extracts metadata from all DICOM files in a directory, and returns a dataframe of all metadata.

        :param directory: Directory to search for DICOM files. Default is DICOM_DIRECTORY.
        :type directory: str, optional
        :param file_type: Type of the DICOM files (either 'dcm' or 'json'). Default is FILE_TYPE.
        :type file_type: str, optional
        :param num_threads: Number of threads to use for concurrent execution. Default is 20.
        :type num_threads: int, optional
        :return: DataFrame of all metadata extracted from the DICOM files in the directory.
        :rtype: pd.DataFrame
        """
    datasets = {}

    file_paths = create_image_list(directory, file_type)

    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        futures = [executor.submit(extract_dicom_metadata, file_path, file_type) for file_path in file_paths]

        for future in tqdm(concurrent.futures.as_completed(futures), total=len(futures)):
            datasets.update(future.result())

    return pydicom_to_df(datasets)


if __name__ == "__main__":
    df = extract_metadata_from_dicoms()
    df.to_csv(os.path.join(f"../metadata/dicom_metadata/{DATASET}_dicom_metadata.csv"), index=False)
