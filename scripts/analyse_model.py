"""
analyse_model.py: Functions and classes for analysing the performance of the CXR model and the pneumothorax cases in
the CXR dataset.

This script contains code for analysing the performance of the CXR model on the external test sets, and for
analysing the pneumothorax cases in the CXR dataset. The script will generate figures and tables for the paper, with
additional figures and tables generated for analysis.  This script is implemented in the analyse_ptx.ipynb notebook.

This code was primarily published to accompany our research letter to offer transparency and the ability to reproduce
our results. Please note that should this code be used for further analysis, the paths to the data and the model
predictions will need to be updated.

Main Components:
- ModelEval: Class for evaluating the pneumothorax model performance on the holdout data.
- InferenceEval: Class for evaluating the demographics of the datasets, the model predictions, and the predicted
    incidence of COVID-19 pneumothorax.
- AnalysePositiveCases: Class to analyse the cases with confirmed pneumothorax.

Author: ANON
Contact email: ANON
Copyright: ANON - SEE LICENSE
Lisence: MIT
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os
from sklearn.utils import resample
from sklearn.metrics import roc_auc_score, precision_score, recall_score, f1_score, average_precision_score, \
    precision_recall_curve, confusion_matrix, roc_curve, auc
from typing import Dict, List, Tuple, Union, Optional
from matplotlib.figure import Figure
from matplotlib.axes import Axes
import matplotlib.pylab as pylab
from mlxtend.plotting import plot_confusion_matrix
from scipy.stats import gaussian_kde, ttest_ind_from_stats
import math
import statsmodels.api as sm


# DIRECTORIES ---------------------------------------------------------------------------------------------------------

# CXR Model Path
RESULTS_PATH = "results/cxr_inference_results.csv"

# GT_PATH
GT_PATH = "results/pthx_ground_truth.csv"

# CXR Review Path
REVIEW_PATH = "results/cxr_review_results.csv"

QC_DIR = 'metadata/qc_data'

CXR_DATES = "metadata/dicom_metadata/cxr_dates.csv"

# NCCID
NCCID_METADATA_PATH = "metadata/dicom_metadata/nccid_dicom_metadata.csv"
NCCID_CLINICAL_DATA_PATH = "metadata/clinical_data/nccid_clinical_data.csv"

# CUH
CUH_METADATA_PATH = "metadata/dicom_metadata/cuh_dicom_metadata.csv"
CUH_DEMOGS_PATH = "metadata/clinical_data/cuh/DEMOGS_V2022-02-11.csv"
CUH_TESTS_PATH = "metadata/clinical_data/cuh/TESTS_V2022-02-11_positive.csv"
CUH_ADT_PATH = "metadata/clinical_data/cuh/ADT_v2022-02-11_positive.csv"

# BIMCV
BIMCV_METADATA_PATH = "metadata/dicom_metadata/bimcv_dicom_metadata.csv"
BIMCV_DEMOGS_PATH = "metadata/clinical_data/bimcv/participants.tsv"
BIMCV_TESTS_PATH = "metadata/clinical_data/bimcv/sil_reg_covid_posi.tsv"

# RICORD
RICORD_METADATA_PATH = "metadata/dicom_metadata/ricord_dicom_metadata.csv"
RICORD_CLINICAL_DATA_PATH = "metadata/clinical_data/ricord_clinical_data.xlsx"


# PLOTTING DEFAULTS ----------------------------------------------------------------------------------------------------

params = {'legend.fontsize': 'x-large',
          'legend.title_fontsize': 'x-large',
          'figure.figsize': (7, 5.5),
          'axes.labelsize': 'x-large',
          'axes.titlesize': 16,
          'figure.titlesize': 16,
          'xtick.labelsize': 'large',
          'ytick.labelsize': 'large',
          'axes.labelpad': 6,
          'axes.labelweight': 'bold',
          'font.family': 'Avenir Next',
          'font.weight': 'bold',
          'savefig.transparent': True,
          'savefig.dpi': 200
          }

pylab.rcParams.update(params)

# Make confusion matrix colors correspond to main colours
CONFUSION_MATRIX_COLORS = {
    'all_holdout': 'Greys',  # greys
    'All Datasets': 'Greys',
    'holdout': 'Greys',
    'cuh': 'Reds',  # red
    'CUH': 'Reds',
    'bimcv': 'Blues',  # blue
    'BIMCV': 'Blues',
    'ricord': 'Oranges',  # oranges
    'RICORD-1C': 'Oranges',
    'nccid': 'RdPu',  # pink
    'NCCID': 'RdPu',
}

# Default figure sizes
DEFAULT_FIGSIZE = (6, 6)
DEFAULT_FIGSIZE_SMALL = (6, 6)
DEFAULT_LARGE_FIGSIZE = (14, 21)
CONFUSION_MATRIX_FIGSIZE = (6, 6)
DEFAULT_IMAGE_GRID_SIZE_FACTOR = 5

pal = sns.color_palette("Set2")
sns.palplot(pal)


# CONSTANTS -----------------------------------------------------------------------------------------------------------

UNSUITABLE_FEATURES = ['nchest', 'paeds', 'de', 'lat', 'rot']

ALL_HOLDOUT = ['all_holdout', 'nccid', 'bimcv', 'cuh']

ALL_INFERENCE = ['all_holdout', 'nccid', 'bimcv', 'cuh', 'ricord']

PRESENTATION_LABEL_MAP = {
    "nccid": "NCCID",
    "cuh": "CUH",
    "bimcv": "BIMCV",
    "ricord": "RICORD",
    "all_holdout": "All Datasets"
}

COLORS = {
    'all_holdout': pal[7],  # grey
    'holdout': pal[7],
    'cuh': pal[1],  # red
    'bimcv': pal[2],  # blue
    'ricord': pal[3],  # orange
    'nccid': pal[4],  # pink
    '0': pal[1],  # brown
    0: pal[1],
    '1': pal[6],  # light blue
    1: pal[6],
}

model_eval_cols = {
    'n': 'n (Ptx)',
    'recall': 'Sensitivity',
    'specificity': 'Specificity',
    'ppv': 'PPV',
    'npv': 'NPV',
    'f1': 'F1 Score',
}


# CLASSES -------------------------------------------------------------------------------------------------------------

class ModelEval:
    """
    Class for evaluating the pneumothorax model performance on the holdout data.

    :param dataset_names: list of dataset names to evaluate on
    :type dataset_names: list
    :param gt_path: path to ground truth csv
    :type gt_path: str
    :param threshold: model threshold
    :type threshold: float
    :param nbootstrap: number of bootstrap samples to use for confidence intervals
    :type nbootstrap: int
    :param prob_col: name of column containing predicted probabilities
    :type prob_col: str
    :param pred_col: name of column containing predicted labels
    :type pred_col: str
    :param true_col: name of column containing ground truth labels
    :type true_col: str
    """
    def __init__(self, dataset_names: List, gt_path: Optional[str] = GT_PATH, threshold: Optional[float] = 0.5,
                 nbootstrap: Optional[int] = 1000, prob_col: Optional[str] = "prob", pred_col: Optional[str] = "pred",
                 true_col: Optional[str] = "gt"):
        self.ds_names = ALL_HOLDOUT if dataset_names == 'all_holdout' else [dataset_names, ] if isinstance(
            dataset_names, str) else dataset_names
        self.true_col: str = true_col
        self.pred_prob_col = prob_col
        self.pred_col = pred_col
        self.threshold: float = threshold
        self.datasets: Dict[str, pd.DataFrame] = self._get_datasets(gt_path)
        self.size_b4_exclusion = {name: len(ds) for name, ds in self.datasets.items()}
        self.metrics: List[str] = [
            'accuracy',
            'roc_auc',
            'precision',
            'recall',
            'sensitivity',
            'specificity',
            'ppv',
            'npv',
            'f1',
            'auprc',
        ]
        self.nbootstrap: int = nbootstrap
        self.datasets = _get_binary_predictions(self.datasets, self.threshold, self.pred_col, self.pred_prob_col)
        self.bootstrap_scores: Dict[str, Dict[str, List[float]]] = {
            name: self._get_scores(name, self.nbootstrap) for name in self.datasets.keys()
        }
        self.cis: Dict[str, Dict[str, Tuple[float, float]]] = {
            name: self._get_cis(name) for name in self.datasets.keys()
        }
        self.results: Dict[str, Dict[str, float]] = {
            name: self._get_results(name) for name in self.datasets.keys()
        }
        self.lengths = {
            name: len(np.array(self.datasets[name][self.true_col].astype(int))) for name in self.datasets.keys()
        }
        self.pneumos = {
            name: len(np.array(self.datasets[name][self.true_col].astype(int))[
                          np.array(self.datasets[name][self.true_col].astype(int)) == 1]) for name in
            self.datasets.keys()
        }

    @property
    def results_df(self) -> pd.DataFrame:
        """
        Get a DataFrame containing the evaluation results along with their confidence intervals.  Note that the entries
        are strings to improve formatting, so you may wish to use the raw values in the self.results and self.cis
        attributes for any plots or additional calculations.

        :return: A pandas DataFrame containing the evaluation results and confidence intervals.
        :rtype: pd.DataFrame
        """
        return _make_results_df(self.results, self.cis, self.lengths, self.pneumos)

    def roc_curve(self, figsize: Optional[Tuple[int]] = DEFAULT_FIGSIZE) -> Tuple[Figure, Axes]:
        """
        Plot the ROC curve.

        :param figsize: The size of the plot. Defaults to DEFAULT_FIGSIZE.
        :type figsize: Tuple[int], optional
        :return: A tuple containing the matplotlib Figure and Axes objects.
        :rtype: Tuple[Figure, Axes]
        """
        fig, ax = plt.subplots(figsize=figsize)
        for name, df in self.datasets.items():
            y_true = df[self.true_col]
            if len(np.unique(y_true)) == 1:
                print(f'No positive samples in {name} dataset. Skipping ROC curve.')
            else:
                y_pred = df[self.pred_prob_col]
                fpr, tpr, _ = roc_curve(y_true, y_pred)
                roc_auc = auc(fpr, tpr)

                ax.plot(fpr, tpr,
                        color=COLORS[name], alpha=1,
                        label=f'{PRESENTATION_LABEL_MAP[name]} (AUC: {roc_auc:.2f})')

        ax.plot([0, 1], [0, 1], 'k--', label='Random (AUC: 0.50)')
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.0])
        ax.set_xlabel('False Positive Rate (1 - Specificity)')
        ax.set_ylabel('True Positive Rate (Sensitivity)')
        ax.set_title('ROC Curves')
        ax.legend(loc='lower right')
        return fig, ax

    def pr_curve(self, figsize: Optional[Tuple[int]] = DEFAULT_FIGSIZE) -> Tuple[Figure, Axes]:
        """
        Plot the precision-recall (PR) curve.

        :param figsize: The size of the plot. Defaults to DEFAULT_FIGSIZE.
        :type figsize: Tuple[int], optional
        :return: A tuple containing the matplotlib Figure and Axes objects.
        :rtype: Tuple[Figure, Axes]
        """
        fig, ax = plt.subplots(figsize=figsize)
        for name, df in self.datasets.items():
            y_true = df[self.true_col]
            if len(np.unique(y_true)) == 1:
                print(f'No positive samples in {name} dataset. Skipping PR curve.')
            else:
                y_pred_prob = np.array(df[self.pred_prob_col])
                precision, recall, threshold_pr = precision_recall_curve(y_true, y_pred_prob)
                ax.plot(recall, precision, color=COLORS[name], alpha=0.9,
                        label=f'{PRESENTATION_LABEL_MAP[name]} (AUC: %0.2f)' % auc(recall, precision))

        ax.set_xlabel('Recall')
        ax.set_ylabel('Precision')
        # ax.set_title('Precision-recall (PR) Curve')
        ax.legend(loc="lower left")
        return fig, ax

    def confusion_matrices(
            self,
            figsize: Optional[Tuple[int]] = CONFUSION_MATRIX_FIGSIZE
    ) -> Dict[str, Tuple[Figure, Axes]]:
        """
        Get the confusion matrix and plot it.

        :param figsize: The size of the plot. Defaults to CONFUSION_MATRIX_FIGSIZE.
        :type figsize: Tuple[int], optional
        :return: A tuple containing the figure and axes of the confusion matrix plot.
        :rtype: Tuple[Figure, Axes]
        """
        matrices = {}
        for name, df in self.datasets.items():
            y_true = df[self.true_col]
            y_pred = np.array(df[self.pred_col])

            conf_matrix = confusion_matrix(y_true, y_pred)
            fig, ax = _make_confusion_matrix(PRESENTATION_LABEL_MAP[name], conf_matrix, figsize=figsize)
            matrices[name] = (fig, ax)
        return matrices

    def _get_datasets(self, gt_path: str) -> Dict[str, pd.DataFrame]:
        """
        Load the specified datasets and, if necessary, calibrate the predictions.

        :param gt_path: The path to the ground truth CSV file.
        :type gt_path: str
        :return: A dictionary containing the dataset DataFrames.
        :rtype: Dict[str, pd.DataFrame]
        :raises ValueError: If no datasets were specified or if an invalid dataset name is provided.
        """
        if len(self.ds_names) == 0:
            raise ValueError("No datasets were specified.")
        else:
            df_holdout = pd.read_csv(RESULTS_PATH)
            ground_truth = pd.read_csv(gt_path)
            df_holdout = ground_truth.merge(df_holdout[['name', 'prob']], on='name', how='left')
            df_holdout['gt'] = df_holdout['gt'].astype(int)
            return _read_datasets(self.ds_names, df_holdout)

    def _get_scores(self, name: str, n_bootstrap: Optional[int] = 1000) -> Dict[str, Dict[str, List[float]]]:
        """
        Compute the scores for the model using bootstrapping and store the results.

        :param name: The name of the dataset for which to calculate the scores.
        :type name: str
        :param n_bootstrap: The number of bootstrap samples to generate. Default is 1000.
        :type n_bootstrap: int, optional
        :return: A dictionary containing the computed scores for each metric.
        :rtype: Dict[str, Dict[str, List[float]]]
        """
        scores = {metric: [] for metric in self.metrics}

        y_true = np.array(self.datasets[name][self.true_col].astype(int))
        y_pred_prob = np.array(self.datasets[name][self.pred_prob_col])
        y_pred = np.array(self.datasets[name][self.pred_col].astype(int))

        if len(np.unique(y_true)) == 1:
            print(f"WARNING: Only one class present in {name}. Can only calculate accuracy.")

        for i in range(n_bootstrap):
            y_true_sample, y_pred_sample, y_pred_prob_sample = resample(y_true, y_pred, y_pred_prob)
            metrics_sample = _compute_metrics(y_true_sample, y_pred_sample, y_pred_prob_sample)

            for metric, value in metrics_sample.items():
                scores[metric].append(value)

        return scores

    def _get_results(self, name: str) -> Dict[str, float]:
        """
        Calculate the mean of the bootstrap scores for each metric.

        :param name: The name of the dataset for which to calculate the results.
        :type name: str
        :return: A dictionary containing the mean of the bootstrap scores for each metric.
        :rtype: Dict[str, float]
        """
        return {metric: np.nanmean(self.bootstrap_scores[name][metric]) for metric in self.metrics}

    def _get_cis(self, name: str) -> Dict[str, Dict[str, Tuple[float, float]]]:
        """
        Get the confidence intervals for each metric.

        :param name: The name of the dataset for which to calculate the confidence intervals.
        :type name: str
        :return: A dictionary containing the confidence intervals.
        :rtype: Dict[str, Dict[str, Tuple[float, float]]]
        """
        return {metric: _get_ci_bounds(self.bootstrap_scores, name, metric) for metric in self.metrics}

    def make_article_tables(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Make the tables for the research letter.

        :return: A dataframe containing the full results and a smaller dataframe containing the more focused results.
        :rtype: Tuple[pd.DataFrame, pd.DataFrame]
        """
        df = self.results_df

        df = df.rename(model_eval_cols).drop('sensitivity')
        for ind in df.index:
            if ind not in model_eval_cols.values():
                df = df.drop(ind)

        full_results = df.copy()

        reduced_table = df.drop(['NCCID', 'BIMCV', 'CUH'], axis=1)

        return full_results, reduced_table


class InferenceEval:
    """
    Class for evaluating the demographics of the datasets, the model predictions, and the predicted incidence of
    COVID-19 pneumothorax.

    :param dataset_names: The names of the datasets to evaluate. Can be a single dataset name, a list of dataset names,
        or 'all_holdout' to evaluate all holdout datasets.
    :type dataset_names: Union[str, List[str]]
    :param threshold: The threshold to use for the model predictions. Default is 0.5.
    :type threshold: float, optional
    :param nbootstrap: The number of bootstrap samples to generate. Default is 1000.
    :type nbootstrap: int, optional
    :param covid_status: The COVID-19 status of the required patients from the datasets. Default is None.
    :type covid_status: Union[str, None]
    :param pcr_interval: The interval of days to use for COVID-positivity around the PCR test. Default is (14, 90).
    :type pcr_interval: Tuple[int, int], optional
    :param prob_col: The name of the column containing the predicted probabilities. Default is 'prob'.
    :type prob_col: str, optional
    :param pred_col: The name of the column containing the predicted labels. Default is 'pred'.
    :type pred_col: str, optional
    :param conf_col: The name of the column containing the pneumothorax confirmation status. Default is
        'pneumothorax_confirmed'.
    :type conf_col: str, optional
    """
    def __init__(self, dataset_names: Union[str, List[str]], threshold: Optional[float] = 0.5,
                 nbootstrap: Optional[int] = 1000, covid_status: Optional[str] = None,
                 pcr_interval: Optional[Tuple[int, int]] = (14, 90), prob_col: Optional[str] = "prob",
                 pred_col: Optional[str] = "pred", conf_col: Optional[str] = "pneumothorax_confirmed"):
        self.ds_names = ALL_INFERENCE if dataset_names == 'all_holdout' else [dataset_names, ] if isinstance(
            dataset_names, str) else dataset_names
        self.pred_prob_col = prob_col
        self.pred_col = pred_col
        self.conf_col = conf_col
        self.threshold: float = threshold
        self.datasets: Dict[str, pd.DataFrame] = self._get_datasets()
        self.datasets = _get_demogs(self.datasets)
        self.original_data = self.datasets.copy()
        self.length_b4_exclusion = {name: len(ds) for name, ds in self.datasets.items()}
        self.datasets = _exclude_unsuitable_cases(self.datasets)
        if covid_status is None:
            pass
        elif covid_status == 'positive' or covid_status == 'negative':
            self.datasets, self.removed, self.included = _select_covid_status(
                self.datasets, covid_status, pcr_int=pcr_interval
            )
        else:
            raise ValueError(
                f"Incorrect covid_status entry ({covid_status}) - please leave as None or enter 'positive' or "
                f"'negative'.")

        self.metrics: List[str] = [
            'incidence_original',
            'incidence_confirmed_cases',
            'adjusted_incidence_bootstrapping'
        ]
        self.nbootstrap: int = nbootstrap
        self.datasets = _get_binary_predictions(self.datasets, self.threshold, self.pred_col, self.pred_prob_col)
        self.all_data = self.datasets.copy()
        self._select_one_per_patient()
        self.bootstrap_scores: Dict[str, Dict[str, List[float]]] = {
            name: self._get_scores(name, self.nbootstrap) for name in self.datasets.keys()
        }
        self.cis: Dict[str, Dict[str, Tuple[float, float]]] = {
            name: self._get_cis(name) for name in self.datasets.keys()
        }
        self.results: Dict[str, Dict[str, float]] = {
            name: self._get_results(name) for name in self.datasets.keys()
        }

    @property
    def results_df(self) -> pd.DataFrame:
        """
        Get a DataFrame containing the evaluation results along with their confidence intervals.  Note that the entries
        are strings to improve formatting, so you may wish to use the raw values in the self.results and self.cis
        attributes for any plots or additional calculations.

        :return: A pandas DataFrame containing the evaluation results and confidence intervals.
        """
        return _make_results_df(self.results, self.cis)

    def _get_datasets(self) -> Dict[str, pd.DataFrame]:
        """
        Load the specified datasets and, if necessary, calibrate the predictions.

        :return: A dictionary containing the dataset DataFrames.
        :raises ValueError: If no datasets were specified or if an invalid dataset name is provided.
        """
        if len(self.ds_names) == 0:
            raise ValueError("No datasets were specified.")
        else:
            df_inf = pd.read_csv(RESULTS_PATH)

            df_review = pd.read_csv(REVIEW_PATH)
            df_review[self.conf_col] = df_review[self.conf_col].map({"Y": 1, "N": 0, "?N": 0, "N?": 0})

            df_inf = df_inf.merge(df_review[['name', self.conf_col]], how='left', on='name')
            df_inf.loc[df_inf[self.conf_col].isna(), self.conf_col] = 0

            return _read_datasets(self.ds_names, df_inf)

    def _get_scores(self, name: str, n_bootstrap: Optional[int] = 1000) -> Dict[
        str, Dict[str, List[float]]]:
        """
        Compute the scores for the model using bootstrapping and store the results.

        :param name: The name of the dataset for which to calculate the scores.
        :type name: str
        :param n_bootstrap: The number of bootstrap samples to generate. Default is 1000.
        :type n_bootstrap: int, optional
        :return: A dictionary containing the computed scores for each metric.
        :rtype: Dict[str, Dict[str, List[float]]]
        """
        scores = {metric: [] for metric in self.metrics}

        y_pred = np.array(self.datasets[name][self.pred_col].astype(int))
        y_conf = np.array(self.datasets[name][self.conf_col].astype(int))

        for i in range(n_bootstrap):
            y_pred_sample, y_conf_sample = resample(y_pred, y_conf)
            metrics_sample = self._compute_incidence(y_pred_sample, y_conf_sample)

            for metric, value in metrics_sample.items():
                scores[metric].append(value)

        return scores

    def _get_results(self, name: str) -> Dict[str, float]:
        """
        Calculate the mean of the bootstrap scores for each metric.

        :param name: The name of the dataset for which to calculate the results.
        :type name: str
        :return: A dictionary containing the mean of the bootstrap scores for each metric.
        :rtype: Dict[str, float]
        """
        return {metric: np.nanmean(self.bootstrap_scores[name][metric]) for metric in self.metrics}

    def _get_cis(self, name: str) -> Dict[str, Dict[str, Tuple[float, float]]]:
        """
        Get the confidence intervals for each metric.

        :param name: The name of the dataset for which to calculate the confidence intervals.
        :type name: str
        :return: A dictionary containing the confidence intervals.
        :rtype: Dict[str, Dict[str, Tuple[float, float]]]
        """
        return {metric: _get_ci_bounds(self.bootstrap_scores, name, metric) for metric in self.metrics}

    def _select_one_per_patient(self):
        """
        Select one cxr for each patient, preferentially keeping confirmed pneumothoraces.
        """
        for name, df in self.datasets.items():
            df = df.sort_values([self.pred_col, self.conf_col], ascending=False).reset_index(drop=True)
            df = df.drop_duplicates(subset=["patientid"], keep='first')
            self.datasets[name] = df.copy()

    @staticmethod
    def _compute_incidence(
            y_pred_sample: np.ndarray,
            y_conf_sample: np.ndarray,
            model_sensitivity: Optional[float] = 0.74,
    ) -> Dict[str, float]:
        """
        Compute the incidence of pneumothorax for the given sample.

        :param y_pred_sample: Array of predictions for the sample images.
        :type y_pred_sample: np.ndarray
        :param y_conf_sample: Array of if pneumothorax confirmed, 1, or not confirmed, 0 for the sample images.
        :type y_conf_sample: np.ndarray
        :param model_sensitivity: The sensitivity of the model. Default is 0.74, the maximum sensitivity for the
        datasets used to test the model. Adjustment assumes that the model does not have a higher sensitivity /
        outperform than this value.
        :type model_sensitivity: float, optional
        :return: A dictionary containing the incidence of pneumothorax for the sample, which includes the predicted
        incidence, the incidence of confirmed cases after radiologist review, and the adjusted incidence.
        :rtype: Dict[str, float]
        """
        return {
            "incidence_original": (len(y_pred_sample[y_pred_sample == 1]) * 100 / len(y_pred_sample)),
            "incidence_confirmed_cases": (len(y_conf_sample[y_conf_sample == 1]) * 100 / len(y_conf_sample)),
            "adjusted_incidence_bootstrapping": (len(y_conf_sample[y_conf_sample == 1]) * 100 / len(
                y_conf_sample)) / model_sensitivity
        }

    def make_demogs_table(self, stage: Optional[str] = "Full Dataset") -> pd.DataFrame:
        """
        Make a table of demographics for the given dataset, including patient demographics and dataset
        characteristics.

        :param stage: The name of the dataset for which to make the table. Default is "Full Dataset". Other options are
            "COVID-positive" and "Pthx Confirmed".
        :type stage: str
        :return: The demographics table for the chosen data.
        :rtype: pd.DataFrame
        :raises ValueError: If the stage is not one of the valid options.
        """
        if stage == "Full Dataset":
            df_all_scans = self.original_data['all_holdout'].copy()
        elif stage == "COVID-positive":
            df_all_scans = self.all_data['all_holdout'].copy()
        elif stage == "Pthx Confirmed":
            df_all_scans = self.all_data['all_holdout'].copy()
            df_all_scans = df_all_scans[df_all_scans['pneumothorax_confirmed'] == 1]
        else:
            raise ValueError(f"Invalid stage: {stage}")

        original_age_info = df_all_scans.groupby('dataset').age.describe()
        total_xrays = original_age_info[['count']].rename({'count': 'No. of CXRs'}, axis=1)
        total_xrays = total_xrays.transpose()
        total_xrays['all_holdout'] = len(df_all_scans)
        total_xrays.loc["No. of CXRs", :] = total_xrays.loc["No. of CXRs", :].apply(lambda x: f"{x:,.0f}")

        # one per patient
        df = df_all_scans.drop_duplicates(subset='patientid')

        age_info = df.groupby('dataset').age.describe()
        overall = df[['age']].describe().transpose()
        overall = overall.rename({'age': 'all_holdout'})
        age_info = pd.concat([age_info, overall])

        sex_info = df.groupby('dataset').sex.value_counts().unstack()
        overall = df.sex.value_counts()
        sex_info.loc['all_holdout',] = overall

        original_demogs = pd.concat([age_info, sex_info], axis=1)
        original_demogs['Male / Female'] = original_demogs.M.apply(
            lambda x: f"{x:,.0f}") + " / " + original_demogs.F.apply(lambda x: f"{x:,.0f}")
        original_demogs["Mean Age (IQR)"] = original_demogs['mean'].apply(lambda x: f"{x:,.1f}") + " (" + \
                                            original_demogs['25%'].apply(lambda x: f"{x:,.0f}") + "-" + original_demogs[
                                                '75%'].apply(lambda x: f"{x:,.0f}") + ")"
        original_demogs = original_demogs.rename({'count': 'No. of Patients'}, axis=1)
        original_demogs['No. of Patients'] = original_demogs['No. of Patients'].apply(lambda x: f'{x:,.0f}')
        original_demogs = original_demogs[['No. of Patients', 'Male / Female', 'Mean Age (IQR)']].transpose()

        final_df = pd.concat([total_xrays, original_demogs])
        final_df["Subset"] = stage
        return final_df.reset_index().set_index(["Subset", 'index'])

    def demogs_table(self) -> pd.DataFrame:
        """
        Make a table of demographics for all datasets.

        :return: The demographics table for all datasets.
        :rtype: pd.DataFrame
        """
        tables = []
        for stage in ["Full Dataset", "COVID-positive", "Pthx Confirmed"]:
            tables.append(self.make_demogs_table(stage))
        return pd.concat(tables).rename(PRESENTATION_LABEL_MAP, axis=1)

    def full_demogs(self) -> pd.DataFrame:
        """
        Make a table of overview demographics for all datasets, including the number of centres and whether reports or
        previously curated labels were available.

        :return: The overview demographics table for all datasets.
        :rtype: pd.DataFrame
        """
        df = self.make_demogs_table("Full Dataset")
        df = df.reset_index().set_index('index')
        df.loc['Centres/Locations',] = np.nan
        df.loc['Reports?',] = np.nan
        df.loc['Centres/Locations', 'all_holdout'] = "-"
        df.loc['Centres/Locations', 'bimcv'] = "11 / Spain"
        df.loc['Centres/Locations', 'cuh'] = "1 / UK"
        df.loc['Centres/Locations', 'ricord'] = "4 / US,CA,BR,TR"
        df.loc['Centres/Locations', 'nccid'] = "10 / UK"
        df.loc['Centres/Locations', 'Subset'] = "Full Dataset"

        df.loc['Reports?', 'all_holdout'] = "-"
        df.loc['Reports?', 'bimcv'] = "NLP Labels"
        df.loc['Reports?', 'cuh'] = "Yes"
        df.loc['Reports?', 'ricord'] = "No"
        df.loc['Reports?', 'nccid'] = "No"
        df.loc['Reports?', 'Subset'] = "Full Dataset"
        return df.reset_index().set_index(['Subset', 'index'])


class AnalysePositiveCases:
    """
    Class to analyse the positive cases in the inference set.

    :param dataset_names: The name of the dataset(s) to analyse.
    :type dataset_names: str or List
    :param pcr_interval: The range of days from the positive COVID-19 test to include in the analysis.
    :type pcr_interval: Tuple
    :param prob_col: The name of the column containing the predicted probability of pneumothorax.
    :type prob_col: str
    :param pred_col: The name of the column containing the predicted label of pneumothorax.
    :type pred_col: str
    :param conf_col: The name of the column containing the confirmed label of pneumothorax.
    :type conf_col: str
    """
    def __init__(self,
                 dataset_names: Optional[Union[str, List[str]]] = 'all_holdout',
                 pcr_interval: Optional[Tuple[int, int]] = (14, 90),
                 prob_col: Optional[str] = "prob",
                 pred_col: Optional[str] = "pred",
                 conf_col: Optional[str] = "pneumothorax_confirmed"
                 ):
        self.ds_names = ALL_INFERENCE if dataset_names == 'all_holdout' else [dataset_names, ] if isinstance(
            dataset_names, str) else dataset_names
        self.pred_prob_col = prob_col
        self.pred_col = pred_col
        self.conf_col = conf_col
        self.datasets: Dict[str, pd.DataFrame] = self._get_datasets()
        self.original_data = self.datasets.copy()
        self.datasets = _exclude_unsuitable_cases(self.datasets)
        self.datasets, self.removed, self.included = _select_covid_status(
            self.datasets, 'positive', pcr_int=pcr_interval
        )
        self.all_scans = self.datasets.copy()
        self._select_one_per_patient()
        self.datasets = _get_demogs(self.datasets)
        self._get_time_interval_floats()
        self.intubated, self.not_intubated = self._split_by_intubation_status()
        self._map_covid_severity()
        self._add_vent_status()

    def _get_datasets(self) -> Dict[str, pd.DataFrame]:
        """
        Load the specified datasets and, if necessary, calibrate the predictions.

        :return: A dictionary containing the dataset DataFrames.
        :raises ValueError: If no datasets were specified or if an invalid dataset name is provided.
        """
        if len(self.ds_names) == 0:
            raise ValueError("No datasets were specified.")
        else:
            df_inf = pd.read_csv(RESULTS_PATH)
            df_review = pd.read_csv(REVIEW_PATH)
            df_review[self.conf_col] = df_review[self.conf_col].map({"Y": 1, "N": 0, "?N": 0, "N?": 0})
            df_inf = df_inf.merge(df_review[[
                'name', self.conf_col, 'covid_severity', 'intubated',
            ]], how='left', on='name')
            df_inf = df_inf[df_inf[self.conf_col] == 1].reset_index()
            return _read_datasets(self.ds_names, df_inf)

    def _select_one_per_patient(self):
        for name, df in self.datasets.items():
            if 'scan_test_interval' not in df.columns:
                df['scan_test_interval'] = np.nan
            df = df.sort_values('scan_test_interval', ascending=True).reset_index(drop=True)
            df = df.drop_duplicates(subset=["patientid"], keep='first')
            self.datasets[name] = df.copy()

    def _get_time_interval_floats(self):
        for name in self.datasets.keys():
            if name != 'ricord':
                self.datasets[name]['scan_test_interval_float'] = self.datasets[name]['scan_test_interval'].dt.days
                self.datasets[name]['scan_test_interval_float_abs'] = self.datasets[name]['scan_test_interval_float'].abs()

    def _split_by_intubation_status(self):
        intubated = {}
        not_intubated = {}
        for name in self.datasets.keys():
            intubated['name'] = self.datasets[name][self.datasets[name].intubated.notna() & (
                self.datasets[name].intubated.str.contains("Y"))]
            intubated['name']['vent'] = "IMV"
            not_intubated['name'] = self.datasets[name][self.datasets[name].intubated.notna() & (
                self.datasets[name].intubated.str.contains("N"))]
            not_intubated['name']['vent'] = "No IMV"
        return intubated, not_intubated

    def make_sex_severity_charts(self, save=False, save_path='sex_severity.png'):
        df = self.datasets["all_holdout"].copy()
        fig, ax = plt.subplots(2, 1, figsize=(4, 6))

        # Sex
        df.sex = df.sex.map({
            'M': 'Male',
            'F': 'Female',
        })
        sns.countplot(df, x="sex", hue='vent', ax=ax[0], palette="Blues", order=["Male", "Female"], saturation=.75)
        ax[0].set_xlabel("Sex or Gender")
        ax[0].set_ylabel("No. of Patients")
        ax[0].get_legend().set_title("")
        ax[0].legend_.remove()

        # Severity
        df.covid_severity = df.covid_severity.map({
            'mild': 'Mild',
            'mod': 'Moderate',
            'sev': 'Severe'
        })
        sns.countplot(df, x="covid_severity", hue='vent', ax=ax[1], palette="Blues",
                      order=['Mild', 'Moderate', 'Severe'])
        ax[1].set_xlabel("Radiographic COVID-19\nSeverity")
        ax[1].set_ylabel("No. of Patients")
        ax[1].get_legend().set_title("")
        ax[1].legend_.remove()

        fig.tight_layout()
        if save:
            fig.savefig(save_path)

        return fig, ax

    def make_age_timeint_charts(self, save=False, save_path='age_time.png'):

        fig, ax = plt.subplots(2, 1, figsize=(7.5 * 4 / 6, 6))

        df = self.datasets["all_holdout"].copy()

        # Age
        palette = sns.color_palette("Blues", n_colors=2)
        # desaturate each color in the palette
        desaturated_palette = [sns.desaturate(color, 0.75) for color in palette]

        sns.histplot(df, x="age", hue='vent', ax=ax[0], palette=desaturated_palette, multiple='dodge', shrink=0.8,
                     edgecolor=None, alpha=1)
        ax[0].set_xlabel("Age")
        ax[0].set_ylabel("No. of Patients")
        ax[0].get_legend().set_title("")
        # ax[0].legend_.remove()

        # Time Interval
        hist = sns.histplot(df, x="scan_test_interval_float_abs", hue='vent', ax=ax[1],
                            # kde=True,
                            palette=desaturated_palette, edgecolor=None,
                            multiple='dodge', shrink=0.6, alpha=1,
                            # common_norm=False,
                            # line_kws={'alpha': 1, 'color': palette}
                            )

        for i, ventil in enumerate(df["vent"].dropna().unique()):
            subset = df[df["vent"] == ventil]["scan_test_interval_float_abs"].dropna()
            x_values = np.linspace(subset.min(), subset.max(), 100)
            kde = gaussian_kde(subset)

            # get the maximum bin height for the current hue category
            hue_patches = hist.patches[i::2]  # get every nth patch, starting from i
            max_bin_height = max([patch.get_height() for patch in hue_patches])
            density = kde(x_values)
            density *= max_bin_height / np.max(density)

            plt.plot(x_values, density, color='w', lw=4.5)  # white outline
            plt.plot(x_values, density, color=palette[i], lw=1.5)  # actual KDE line

        ax[1].set_xlabel("Interval between 1st Positive COVID-19 Test\nand 1st Imaging of Pneumothorax (days)")
        ax[1].set_ylabel("No. of Patients")
        ax[1].get_legend().set_title("")

        fig.tight_layout()
        if save:
            fig.savefig(save_path)

        return fig, ax

    @staticmethod
    def make_cis_for_stats(df, field, category):
        # set variables
        n = df[field].value_counts().values.sum()
        if field == "age":
            p = len(df[(df[field] >= category)]) / n
        elif field == 'intubated' or field == 'ethnicity':
            p = len(df[(df[field].fillna("").str.contains(category))])/n
        else:
            p = len(df[(df[field] == category)]) / n
        z = 1.96  # for a 95% confidence interval

        # calculate confidence interval
        lower = p - z * math.sqrt(p * (1 - p) / n)
        upper = p + z * math.sqrt(p * (1 - p) / n)

        if field == "age":
            print(f"The percentage of patients aged {category} or over is {p * 100:.1f}% with a 95% confidence interval of "
                  f"({lower * 100:.1f}%, {upper * 100:.1f}%)")
        else:
            print(f"The percentage of {category} patients is {p * 100:.1f}% with a 95% "
                  f"confidence interval of ({lower * 100:.1f}%, {upper * 100:.1f}%)")

        return lower, upper

    @staticmethod
    def perform_categorical_ttest(group1, group2, field, category):
        # group 1 counts and sample size
        group1_counts = len(group1[group1[field] == category])
        group1_size = group1[field].value_counts().values.sum()

        # group 2 counts and sample size
        group2_counts = len(group2[group2[field] == category])
        group2_size = group2[field].value_counts().values.sum()

        # perform z-test
        z_score, p_value = sm.stats.proportions_ztest([group1_counts, group2_counts], [group1_size, group2_size])

        # print results
        print(f"z-score = {z_score:.3f}, p = {p_value:.5f}")

    @staticmethod
    def perform_ppv_time_int_numeric_ttest(df, remove_lt0=True):
        from scipy.stats import t
        alpha = 0.05  # 95% confidence interval

        # ignore intervals <=0
        if remove_lt0:
            df = df[df.scan_test_interval_float > 0]

        n1 = len(df[df.intubated.notna() & df.intubated.str.contains('Y')])
        mean1 = np.mean(df[df.intubated.notna() & df.intubated.str.contains('Y')].scan_test_interval_float)
        std1 = np.std(df[df.intubated.notna() & df.intubated.str.contains('Y')].scan_test_interval_float)
        se1 = std1 / np.sqrt(n1)
        df_t1 = n1 - 1
        t_score2 = t.ppf(1 - alpha / 2, df=df_t1)
        lower1 = mean1 - t_score2 * se1
        upper1 = mean1 + t_score2 * se1

        # group 2 information
        n2 = len(df[df.intubated.notna() & df.intubated.str.contains('N')])
        mean2 = np.mean(df[df.intubated.notna() & df.intubated.str.contains('N')].scan_test_interval_float_abs)
        std2 = np.std(df[df.intubated.notna() & df.intubated.str.contains('N')].scan_test_interval_float_abs)
        se2 = std2 / np.sqrt(n2)
        df_t2 = n2 - 1
        t_score2 = t.ppf(1 - alpha / 2, df=df_t2)
        lower2 = mean2 - t_score2 * se2
        upper2 = mean2 + t_score2 * se2

        # calculate t and p-value
        t, p = ttest_ind_from_stats(mean1, std1, n1, mean2, std2, n2)

        # print results
        print(f"non-PPV time interval mean = {mean1:.3f} days, with a 95% confidence interval of "
              f"({lower1:.2f}, {upper1:.2f})), and std = {std1:.3f} days, n = {n1}")
        print(f"non-PPV time interval mean = {mean2:.3f} days, with a 95% confidence interval of "
              f"({lower2:.2f}, {upper2:.2f})), and std = {std2:.3f} days, n = {n2}")
        print(f"t = {t:.3f}, p = {p:.3f}")
        return t, p

    def _map_covid_severity(self):
        severity_map = {
            'Mild-Moderate': 'mild-mod',
            'severe': 'sev',
            'Moderate': 'mod',
            'mild': 'mild',
            'mod': 'mod',
            'sev': 'sev',
        }
        for ds in self.datasets.keys():
            self.datasets[ds].loc[self.datasets[ds].covid_severity == 'moderate / 4', "covid_severity"] = "4 / mod"
            self.datasets[ds].covid_severity = self.datasets[ds].covid_severity.str.split("/").str[1].str.strip(" ")
            self.datasets[ds].covid_severity = self.datasets[ds].covid_severity.map(severity_map)

    def _add_vent_status(self):
        for ds in self.datasets.keys():
            self.datasets[ds].loc[(self.datasets[ds]['intubated'].fillna("").str.contains("Y")), 'vent'] = "IMV"
            self.datasets[ds].loc[(self.datasets[ds]['intubated'].fillna("").str.contains("N")), 'vent'] = "No IMV"


# FUNCTIONS ------------------------------------------------------------------------------------------------------------

def _make_confusion_matrix(
        title: str,
        conf_matrix: np.ndarray,
        figsize: Tuple[int] = CONFUSION_MATRIX_FIGSIZE,
) -> Tuple[Figure, Axes]:
    """
    Function to plot the confusion matrix.

    :param title: The title for the confusion matrix plot.
    :param conf_matrix: The confusion matrix as a numpy array.
    :param figsize: The size of the plot. Defaults to CONFUSION_MATRIX_FIGSIZE.
    :return: A tuple containing the figure and axes of the confusion matrix plot.
    """
    fig, ax = plot_confusion_matrix(conf_mat=conf_matrix, figsize=figsize, cmap=CONFUSION_MATRIX_COLORS[title])
    ax.set_title(title)

    return fig, ax


def _calculate_conf_matrix_metrics(conf_matrix: np.ndarray) -> Tuple[float, float, float, float]:
    """
    Calculate the sensitivity, specificity, positive predictive value, and negative predictive value from the
    confusion matrix.

    :param conf_matrix: The confusion matrix as a numpy array.
    :return: A tuple containing the sensitivity, specificity, positive predictive value, and negative predictive
        value.
    """
    tn, fp, fn, tp = conf_matrix.ravel()
    sens = tp / (tp + fn) if (tp + fn) != 0 else 0
    spec = tn / (tn + fp) if (tn + fp) != 0 else 0
    ppv = tp / (tp + fp) if (tp + fp) != 0 else 0
    npv = tn / (tn + fn) if (tn + fn) != 0 else 0

    return sens, spec, ppv, npv


def _compute_metrics(
        y_true_sample: np.ndarray,
        y_pred_sample: np.ndarray,
        y_pred_prob_sample: np.ndarray = None
) -> Dict[str, float]:
    """
    Compute the metrics and return them as a dictionary.

    :param y_true_sample: The true labels for a bootstrap sample.
    :param y_pred_sample: The predicted labels for a bootstrap sample.
    :param y_pred_prob_sample: The predicted probabilities for a bootstrap sample. Defaults to None, in which case
        the metrics that require predicted probabilities will be set to np.nan (i.e. AUROC and AUPRC).
    :return: A dictionary containing computed metrics.
    """
    if len(np.unique(y_true_sample)) == 1:
        return {
            'accuracy': np.mean(y_true_sample == y_pred_sample),
            'sensitivity': np.nan,
            'specificity': np.nan,
            'ppv': np.nan,
            'npv': np.nan,
            'roc_auc': np.nan,
            'auprc': np.nan,
            'precision': np.nan,
            'recall': np.nan,
            'f1': np.nan
        }
    else:
        confusion_matrix_sample = confusion_matrix(y_true_sample, y_pred_sample)
        sensitivity_sample, specificity_sample, ppv_sample, npv_sample = _calculate_conf_matrix_metrics(
            confusion_matrix_sample
        )
        if y_pred_prob_sample is None:
            return {
                "accuracy": np.mean(y_true_sample == y_pred_sample),
                "roc_auc": np.nan,
                "precision": precision_score(y_true_sample, y_pred_sample),
                "recall": recall_score(y_true_sample, y_pred_sample),
                "sensitivity": sensitivity_sample,
                "specificity": specificity_sample,
                "ppv": ppv_sample,
                "npv": npv_sample,
                "f1": f1_score(y_true_sample, y_pred_sample),
                "auprc": np.nan,
            }
        else:
            return {
                "accuracy": np.mean(y_true_sample == y_pred_sample),
                "roc_auc": roc_auc_score(y_true_sample, y_pred_prob_sample),
                "precision": precision_score(y_true_sample, y_pred_sample),
                "recall": recall_score(y_true_sample, y_pred_sample),
                "sensitivity": sensitivity_sample,
                "specificity": specificity_sample,
                "ppv": ppv_sample,
                "npv": npv_sample,
                "f1": f1_score(y_true_sample, y_pred_sample),
                "auprc": average_precision_score(y_true_sample, y_pred_prob_sample),
            }


def _exclude_unsuitable_cases(datasets: Dict[str, pd.DataFrame]) -> Dict[str, pd.DataFrame]:
    """
    Exclude unsuitable cases from each dataset based on the label stage and unsuitable features. These features
    were excluded in training to reduce noise, but you may wish to include them in the predictions to see how they
    are handled by the model as you may not be able to exclude them in practice.

    This method iterates through all datasets and removes cases with unsuitable features according to the
    label stage. The updated datasets are then assigned back to the datasets dictionary.

    :param label: The QC label, e.g. 'inverted' or 'aspect_ratio'.
    :param datasets: A dictionary containing the dataframes to be filtered.
    :return: A dictionary containing the filtered dataframes.
    """

    for i, file in enumerate(os.listdir(QC_DIR)):
        if file.endswith('csv'):
            qc_data = pd.read_csv(os.path.join(QC_DIR, file))

            qc_data = qc_data[["name"] + UNSUITABLE_FEATURES]

            if i == 0:
                qc = qc_data
            else:
                qc = pd.concat([qc, qc_data])

    for name, df in datasets.items():
        df = df.merge(qc, how='left', on='name')
        for feature in UNSUITABLE_FEATURES:
            if feature in df.columns:
                df = df.loc[(df[feature] == '0') | (df[feature] == 0)]
        datasets[name] = df.reset_index(drop=True).copy()

    return datasets


def _format_list_to_string(lst: List[float], dcml_places: int = 2) -> str:
    """
    Format a list of two floats into a string representation.

    :param lst: List of floats to be formatted.
    :param dcml_places: The number of decimal places to round to. Defaults to 3.
    :return: A string representation of the list.
    """
    return f"[{lst[0]:.{dcml_places}f}, {lst[1]:.{dcml_places}f}]"


def _make_results_df(results: Dict, cis: Dict, lengths=None, pneumos=None, ) -> pd.DataFrame:
    """
    Get a DataFrame containing the evaluation results along with their confidence intervals.  Note that the entries
    are strings to improve formatting, so you may wish to use the raw values in the self.results and self.cis
    attributes for any plots or additional calculations.

    :param results: A dictionary containing the evaluation results.
    :param cis: A dictionary containing the confidence intervals.
    :return: A pandas DataFrame containing the evaluation results and confidence intervals.
    """
    if lengths is not None and pneumos is not None:
        lengths_df = pd.DataFrame(lengths, index=['n'])
        pneumos_df = pd.DataFrame(pneumos, index=['pneumos'])
        for col in lengths_df.columns:
            lengths_df[col] = lengths_df[col].astype(str)
            pneumos_df[col] = pneumos_df[col].astype(str)
        lengths_df.iloc[0] = lengths_df.iloc[0].str[:-3] + "," + lengths_df.iloc[0].str[-3:] + " (" + pneumos_df.iloc[
            0] + ")"
    results_df = pd.DataFrame(results)
    cis = pd.DataFrame(cis)
    combined_cols_df = results_df.copy()

    for col in combined_cols_df.columns:
        results_df[col] = results_df[col].apply(lambda x: f"{x:.2f}")
        cis[col] = cis[col].apply(_format_list_to_string)
        combined_cols_df[col] = results_df[col] + " " + cis[col]

    if lengths is not None and pneumos is not None:
        combined_cols_df = pd.concat([lengths_df, combined_cols_df])

    combined_cols_df = combined_cols_df.apply(lambda x: x.replace('nan', ''))
    combined_cols_df = combined_cols_df.apply(lambda x: x.replace('[]', ''))

    label_map = {col: PRESENTATION_LABEL_MAP[col] for col in combined_cols_df.columns}
    combined_cols_df = combined_cols_df.replace("0.00 (0.00, 0.00)", "- (-, -)")

    return combined_cols_df.rename(label_map, axis=1)


def _get_binary_predictions(datasets, threshold, pred_col, pred_prob_col):
    """
    Generate binary predictions based on the chosen threshold attribute.
    """
    for name in datasets.keys():
        datasets[name][pred_col] = 0
        datasets[name].loc[
            (datasets[name].dataset == name) &
            (datasets[name][pred_prob_col].astype(float) >= threshold), pred_col
        ] = 1
    return datasets


def _get_ci_bounds(bootstrap_scores, name: str, metric: str) -> Tuple[float, float]:
    """
    Get the confidence interval bounds for a specific metric.

    :param name: The name of the dataset for which to calculate the confidence interval bounds.
    :param metric: The metric for which to calculate the confidence interval bounds.
    :return: A tuple containing the lower and upper bounds of the confidence interval.
    """
    lower_bound = np.nanpercentile(bootstrap_scores[name][metric], 2.5)
    upper_bound = np.nanpercentile(bootstrap_scores[name][metric], 97.5)
    return lower_bound, upper_bound


def _read_datasets(names, df):
    datasets = {}
    for name in names:
        if name == 'all_holdout':
            datasets['all_holdout'] = df.copy()
        elif name == 'bimcv':
            datasets['bimcv'] = df[df.dataset == 'bimcv'].copy().reset_index(drop=True)
        elif name == 'nccid':
            datasets['nccid'] = df[df.dataset == 'nccid'].copy().reset_index(drop=True)
        elif name == "cuh":
            datasets['cuh'] = df[df.dataset == 'cuh'].copy().reset_index(drop=True)
        elif name == "ricord":
            datasets['ricord'] = df[df.dataset == 'ricord'].copy().reset_index(drop=True)
        else:
            raise ValueError(f"Invalid dataset name: {name}")
    return datasets


def _get_demogs(datasets):
    nccid = pd.read_csv(NCCID_CLINICAL_DATA_PATH)[['Pseudonym', 'age', 'ethnicity', 'sex']]
    nccid = nccid.rename({'Pseudonym': 'patientid'}, axis=1)

    cuh = pd.read_csv(CUH_DEMOGS_PATH)[
        ['STUDY_SUBJECT_DIGEST', 'AGE_IN_YEARS_IF_ALIVE', 'GENDER_DESC', 'ETHNIC_GROUP_DESC']]
    cuh = cuh.rename({'STUDY_SUBJECT_DIGEST': 'patientid', 'AGE_IN_YEARS_IF_ALIVE': 'age', 'GENDER_DESC': 'sex',
                      'ETHNIC_GROUP_DESC': 'ethnicity'}, axis=1)

    ricord = pd.read_excel(RICORD_CLINICAL_DATA_PATH)[['Anon MRN', 'Anon Age', 'Anon Sex']]
    ricord = ricord.rename({'Anon MRN': 'patientid', 'Anon Age': 'age', 'Anon Sex': 'sex'}, axis=1)
    ricord['ethnicity'] = np.nan
    ricord = ricord.drop_duplicates(subset="patientid", keep='first').reset_index(drop=True)

    bimcv = pd.read_csv(BIMCV_DEMOGS_PATH, sep="\t")[['participant', 'age', 'gender']]
    bimcv = bimcv.rename({'participant': 'patientid', 'gender': 'sex'}, axis=1)
    bimcv.age = bimcv.age.astype(str).str.strip("[").str.strip("]").str.split(',').str[0]
    bimcv['ethnicity'] = np.nan

    demogs = pd.concat([nccid, bimcv, ricord, cuh]).reset_index(drop=True)

    demogs.sex = demogs.sex.map({
        "M": "M",
        "F": "F",
        "Female": "F",
        "Male": "M",
    })
    demogs.age = demogs.age.apply(lambda x: np.ceil(float(x)) if pd.notna(x) and x != "" else np.nan)

    for name, df in datasets.items():
        df = df.merge(demogs, on='patientid', how='left')
        datasets[name] = df.copy()

    return datasets


def _get_scan_dates():
    nccid = pd.read_csv(NCCID_METADATA_PATH, low_memory=False)
    cuh = pd.read_csv(CUH_METADATA_PATH, low_memory=False)
    bimcv = pd.read_csv(BIMCV_METADATA_PATH, low_memory=False)

    nccid['name'] = 'nccid_' + nccid['Pseudonym'] + "_" + nccid['SOPInstanceUID'] + ".png"
    cuh['name'] = "cuh_" + cuh.PatientID + "_" + cuh['SOPInstanceUID'] + ".png"
    bimcv['name'] = bimcv['png_filename'].str.replace("val", "bimcv")
    nccid['dataset'] = 'nccid'
    cuh['dataset'] = "cuh"
    bimcv['dataset'] = "bimcv"

    meta = pd.concat([nccid, cuh, bimcv]).reset_index(drop=True).copy()
    meta['scan_date'] = pd.to_datetime(meta.StudyDate, format="%Y%m%d")
    meta.loc[meta.scan_date.isna(), "scan_date"] = pd.to_datetime(meta.AcquisitionDate, format="%Y%m%d")
    meta['patientid'] = meta.name.str.split("_").str[1]
    return meta


def _get_first_positive_pcr_dates():
    nccid_pcrs = pd.read_csv(NCCID_CLINICAL_DATA_PATH, low_memory=False)[
        ['Pseudonym', 'date_of_positive_covid_swab']]
    nccid_pcrs = nccid_pcrs.rename({"Pseudonym": "patientid", "date_of_positive_covid_swab": "positive_swab_date"},
                                   axis=1)
    nccid_pcrs.positive_swab_date = pd.to_datetime(nccid_pcrs.positive_swab_date)

    bimcv_pcrs = pd.read_csv(BIMCV_TESTS_PATH, sep='\t', low_memory=False)
    bimcv_pcrs = bimcv_pcrs.rename({"participant": "patientid"}, axis=1)
    bimcv_pcrs.date = pd.to_datetime(bimcv_pcrs.date, format="%d.%m.%Y")
    bimcv_pcrs.loc[bimcv_pcrs.result == "POSITIVO", 'positive_swab_date'] = bimcv_pcrs.date
    bimcv_pcrs = bimcv_pcrs.sort_values("positive_swab_date").reset_index(drop=True).drop_duplicates(
        subset='patientid', keep='first')
    bimcv_pcrs = bimcv_pcrs[['patientid', 'positive_swab_date']]

    cuh_tests = pd.read_csv(CUH_TESTS_PATH, low_memory=False)
    cuh_pcrs = cuh_tests[
        (cuh_tests["TestName"].str.contains('COV')) |
        (cuh_tests["TestName"].str.contains('CORONAVIRUS')) |
        (cuh_tests["TestGroupName"].str.contains('COV')) |
        (cuh_tests["TestGroupName"].str.contains('CORONAVIRUS')) |
        (cuh_tests["TestName"].str.contains('AZ RESULT')) &
        (cuh_tests["TestGroupName"].str.contains('HCW SCREEN')) |
        (cuh_tests["TestName"].str.contains('AZ RESULT')) &
        (cuh_tests["TestGroupName"].str.contains('HCW MULTISCREEN'))
        ]
    results_dict = {"Detected": "POSITIVE",
                    "Not detected": "NEGATIVE",
                    "POSITIVE": "POSITIVE",
                    "NEGATIVE": "NEGATIVE",
                    "Negative": "NEGATIVE",
                    "Positive": "POSITIVE",
                    "INVALID": "INVALID",
                    "Invalid": "INVALID",
                    "Indeterminate": "INDETERMINATE",
                    "INDETERMINATE": "INDETERMINATE"
                    }
    cuh_pcrs['ResultValue'] = cuh_pcrs['ResultValue'].map(results_dict)
    cuh_pcrs.loc[cuh_pcrs["COLLECTED_DATETIME"].isna(), 'COLLECTED_DATETIME'] = cuh_pcrs["ResultDate"].copy()
    cuh_pcrs.COLLECTED_DATETIME = pd.to_datetime(cuh_pcrs.COLLECTED_DATETIME)
    cuh_pcrs.loc[cuh_pcrs.ResultValue == "POSITIVE", 'positive_swab_date'] = cuh_pcrs['COLLECTED_DATETIME'].copy()
    cuh_pcrs = cuh_pcrs.rename({"STUDY_SUBJECT_DIGEST": "patientid"}, axis=1)
    cuh_pcrs = cuh_pcrs.sort_values('positive_swab_date').reset_index(drop=True)
    cuh_pcrs = cuh_pcrs.drop_duplicates(subset="patientid", keep='first').reset_index(drop=True)

    return pd.concat([
        nccid_pcrs.fillna("Negative"),
        bimcv_pcrs.fillna("Negative"),
        cuh_pcrs.fillna("Negative")
    ]).reset_index(drop=True)


def _select_covid_status(datasets, requested_status, pcr_int=(14, 90)):
    removed = {}
    included = {}

    pos_pcr_dates = _get_first_positive_pcr_dates()
    pos_pcr_dates.positive_swab_date = pos_pcr_dates.positive_swab_date.fillna("Negative")
    scan_dates = _get_scan_dates()
    data = scan_dates.merge(pos_pcr_dates, on='patientid', how='left')

    data.loc[data.positive_swab_date == 'Negative', 'scan_covid_status'] = 'negative'
    data.loc[data.positive_swab_date == 'Negative', 'positive_swab_date'] = np.nan

    def _calculate_timedelta(x):
        if pd.notna(x.scan_date) and pd.notna(x.positive_swab_date):
            return x.scan_date - x.positive_swab_date
        else:
            return np.nan

    for name, df in datasets.items():
        df = df.merge(data[['name', 'scan_date', 'positive_swab_date', ]], how='left', on='name')
        df['scan_test_interval'] = df.apply(_calculate_timedelta, axis=1)

        if name != 'ricord':
            df.loc[
                (df.dataset == 'ricord') | ((df.scan_test_interval <= pd.Timedelta(pcr_int[1], "d")) & (
                        df.scan_test_interval >= pd.Timedelta(-pcr_int[0], "d"))),
                'scan_covid_status'
            ] = 'positive'
        else:
            df['scan_covid_status'] = 'positive'

        # df.loc[df.dataset == 'ricord', "scan_covid_status"] = requested_status
        removed[name] = df[df.scan_covid_status != requested_status].reset_index().copy()
        included[name] = df[df.scan_covid_status == requested_status].copy()
        df = df[df.scan_covid_status == requested_status]
        datasets[name] = df.reset_index(drop=True).copy()

    return datasets, removed, included
